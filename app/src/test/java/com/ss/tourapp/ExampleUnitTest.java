package com.ss.tourapp;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * LocationsPoojo local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void main() throws Exception {

        Helper obj = new Helper();
        thridMethod(obj);

    }

    void firstMethod(String name) {
        System.out.print(name);
    }

    void secondMethod(int a, int b) {
        System.out.print(a + b);
    }


    void thridMethod(Helper obj) {
        List<Integer> userEntries = new ArrayList<>();
        userEntries.add(1);
        userEntries.add(9);
        userEntries.add(5);
        userEntries.add(2);

        int result = obj.addition(userEntries);

        System.out.print(result);
    }

}