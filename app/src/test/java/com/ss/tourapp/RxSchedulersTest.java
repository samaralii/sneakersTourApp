package com.ss.tourapp;

import com.ss.tourapp.injection.modules.RxSchedulersInterface;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Sammie on 3/29/2017.
 */

public class RxSchedulersTest implements RxSchedulersInterface {


    @Override
    public Scheduler io() {
        return Schedulers.trampoline();
    }

    @Override
    public Scheduler computaion() {
        return Schedulers.trampoline();
    }

    @Override
    public Scheduler ui() {
        return Schedulers.trampoline();
    }
}
