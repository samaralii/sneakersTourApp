package com.ss.tourapp.infrastructure;

import android.app.Application;

import com.ss.tourapp.Utilz;
import com.ss.tourapp.injection.AppComponent;
import com.ss.tourapp.injection.DaggerAppComponent;
import com.ss.tourapp.injection.modules.AppModule;
import com.ss.tourapp.injection.modules.DbModule;
import com.ss.tourapp.injection.modules.NetModule;
import com.ss.tourapp.injection.modules.RepositoryModule;
import com.ss.tourapp.injection.modules.SchedulersModule;

import io.realm.Realm;


/**
 * Created by Sammie on 3/22/2017.
 */

public class TourApp extends Application {

    private AppComponent component;


    @Override
    public void onCreate() {
        super.onCreate();
        initDagger();
    }

    private void initDagger() {

        Realm.init(this);

        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(Utilz.Const.BASE_URL))
                .repositoryModule(new RepositoryModule())
                .schedulersModule(new SchedulersModule())
                .dbModule(new DbModule())
                .build();

        component.Inject(this);


    }


    public AppComponent appComponent() {
        return component;
    }


}
