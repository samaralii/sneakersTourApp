package com.ss.tourapp.injection.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;

/**
 * Created by Sammie on 5/3/2017.
 */

@Module
public class DbModule {

    @Provides
    @Singleton
    public Realm provideDb() {
        return Realm.getDefaultInstance();
    }
}
