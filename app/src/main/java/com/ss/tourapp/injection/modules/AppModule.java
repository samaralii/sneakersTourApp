package com.ss.tourapp.injection.modules;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Sammie on 3/24/2017.
 */

@Module
public class AppModule {

    private Context mContext;


    public AppModule(Context mContext) {
        this.mContext = mContext;
    }


    @Provides
    public Context getApplication() {
        return mContext;
    }
}
