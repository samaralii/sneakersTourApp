package com.ss.tourapp.injection;

import com.ss.tourapp.features.splashScreen.SplashScreenActivity;
import com.ss.tourapp.infrastructure.TourApp;
import com.ss.tourapp.injection.modules.AppModule;
import com.ss.tourapp.injection.modules.DbModule;
import com.ss.tourapp.injection.modules.NetModule;
import com.ss.tourapp.injection.modules.RepositoryModule;
import com.ss.tourapp.injection.modules.SchedulersModule;
import com.ss.tourapp.features.detailView.DetailActivity;
import com.ss.tourapp.features.detailView.detailViewKt.DetailActivityKt;
import com.ss.tourapp.features.mainView.MainActivity;
import com.ss.tourapp.features.mapView.MapActivity;
import com.ss.tourapp.features.visitView.VisitActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Sammie on 3/24/2017.
 */

@Singleton
@Component(modules = {AppModule.class, NetModule.class,
        RepositoryModule.class, SchedulersModule.class,
        DbModule.class})
public interface AppComponent {

    void Inject(TourApp tourApp);

    void Inject(MainActivity mainActivity);

    void Inject(VisitActivity visitActivity);

    void Inject(DetailActivity detailActivity);

    void Inject(MapActivity mapActivity);

    void Inject(DetailActivityKt detailActivityKt);

    void Inject(SplashScreenActivity splashScreenActivity);

}