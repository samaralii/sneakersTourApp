package com.ss.tourapp.injection.modules;

import com.ss.tourapp.data.source.TaskRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import retrofit2.Retrofit;

/**
 * Created by Sammie on 3/24/2017.
 */

@Module
public class RepositoryModule {


    @Singleton
    @Provides
    public TaskRepository provideRepo(Retrofit retrofit, Realm realm) {
        return new TaskRepository(retrofit, realm);
    }


}
