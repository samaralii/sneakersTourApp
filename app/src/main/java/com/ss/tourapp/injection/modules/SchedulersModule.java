package com.ss.tourapp.injection.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Sammie on 3/24/2017.
 */

@Module
public class SchedulersModule {

    @Provides
    @Singleton
    public RxSchedulers provideScheduler() {
        return new RxSchedulers();
    }


}
