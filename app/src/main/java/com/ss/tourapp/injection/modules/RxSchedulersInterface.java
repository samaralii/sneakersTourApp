package com.ss.tourapp.injection.modules;

import io.reactivex.Scheduler;

/**
 * Created by Sammie on 3/29/2017.
 */

public interface RxSchedulersInterface {

    Scheduler io();

    Scheduler computaion();

    Scheduler ui();

}
