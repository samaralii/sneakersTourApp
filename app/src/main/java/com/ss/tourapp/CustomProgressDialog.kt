package com.ss.tourapp

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget

/**
 * Created by Sammie on 5/16/2017.
 */

class CustomProgressDialog : DialogFragment() {


    lateinit var unbinder: Unbinder

    @BindView(R.id.loadingImage)
    lateinit var pd: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater?.inflate(R.layout.dialog_view, container, false) as View
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dialog.window.requestFeature(Window.FEATURE_NO_TITLE)

        val glideDrawableImageViewTarget = GlideDrawableImageViewTarget(pd)

        Glide.with(context)
                .load(R.drawable.animatedlogo)
                .into(glideDrawableImageViewTarget)

    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder.unbind()
    }

}