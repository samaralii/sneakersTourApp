package com.ss.tourapp.data.source;

import android.support.annotation.NonNull;

import com.ss.tourapp.data.pojos.CityPojo;
import com.ss.tourapp.data.pojos.ImagesPojo;
import com.ss.tourapp.data.pojos.LocationsPojo;
import com.ss.tourapp.data.pojos.SearchTours;
import com.ss.tourapp.data.pojos.StatesPojo;
import com.ss.tourapp.data.pojos.nearby.NearbyPojo;
import com.ss.tourapp.data.pojos.subLocationPojo.TourLocation;
import com.ss.tourapp.data.source.remote.WebServices;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;
import io.realm.Realm;
import retrofit2.Retrofit;

/**
 * Created by Sammie on 3/22/2017.
 */

@Singleton
public class TaskRepository implements TaskDataSource {

    private WebServices api;
    private Realm realm;


    @Inject
    public TaskRepository(Retrofit retrofit, Realm realm) {
        api = retrofit.create(WebServices.class);
        this.realm = realm;
    }

    @NonNull
    @Override
    public Single<List<LocationsPojo>> getAllLocation(String lat, String lon) {
        return api.getLocationData(lat, lon)
                .map(response -> {

                    if (response == null || !response.isSuccessful())
                        return null;


                    List<LocationsPojo> locationsPojoList = new ArrayList<>();


                    for (int i = 0; i < response.body().size(); i++) {

                        if (response.body().get(i).getTour().getActive().equalsIgnoreCase("1"))
                            locationsPojoList.add(response.body().get(i));

                    }

                    return locationsPojoList;

                });
    }

    @NonNull
    @Override
    public Single<List<TourLocation>> getSubLocations(String id) {
        return api.getSubLocationData(id)
                .map(response -> {

                    if (response == null || !response.isSuccessful())
                        return null;

                    return response.body();

                });
    }

    @NonNull
    @Override
    public Single<NearbyPojo> getNearbyTour(String lat, String lon) {
        return api.getNearbyTour(lat, lon)
                .map(response -> {

                    if (response == null || !response.isSuccessful())
                        throw new Exception("Error : response");

                    return response.body();
                });
    }

    @NonNull
    @Override
    public Single<CityPojo> getCitites(String stateAbbr) {
        return api.getCities(stateAbbr)
                .map(response -> {

                    if (response == null || !response.isSuccessful())
                        throw new Exception("Error : response");


                    return response.body();
                });
    }

    @NonNull
    @Override
    public Single<SearchTours> searchTours(String name, String city, String state) {
        return api.searchTours(name, state, city)
                .map(response -> {

                    if (response == null || !response.isSuccessful())
                        throw new Exception("Error : response");


                    return response.body();
                });
    }

    @NonNull
    @Override
    public Single<ImagesPojo> getImages(String id) {
        return api.getImages(id)
                .map(response -> {

                    if (response == null || !response.isSuccessful())
                        throw new Exception("Error : response");

                    return response.body();
                });
    }

    private void saveStatesInDb() {

        realm.beginTransaction();
        StatesPojo states = realm.createObject(StatesPojo.class);


    }


}
