
package com.ss.tourapp.data.pojos.nearby;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NearbyPojo {

    @SerializedName("tours")
    @Expose
    private List<Tour> tours = null;

    public List<Tour> getTours() {
        return tours;
    }

    public void setTours(List<Tour> tours) {
        this.tours = tours;
    }

}
