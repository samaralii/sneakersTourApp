package com.ss.tourapp.data.source.remote;

import com.ss.tourapp.data.pojos.CityPojo;
import com.ss.tourapp.data.pojos.ImagesPojo;
import com.ss.tourapp.data.pojos.LocationsPojo;
import com.ss.tourapp.data.pojos.SearchTours;
import com.ss.tourapp.data.pojos.locationapipojo.LocationApiPOJO;
import com.ss.tourapp.data.pojos.nearby.NearbyPojo;
import com.ss.tourapp.data.pojos.subLocationPojo.TourLocation;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Sammie on 3/22/2017.
 */

public interface WebServices {

    @GET("tours/all_locations")
    Single<Response<List<LocationsPojo>>> getLocationData(@Query("lat") String lat, @Query("lon") String lon);


    @GET("tours/appapi/{id}")
    Single<Response<List<TourLocation>>> getSubLocationData(@Path("id") String id);

    @GET("json?")
    Single<Response<LocationApiPOJO>> getRoutes(@Query("origin") String origin,
                                                @Query("destination") String destination,
                                                @Query("key") String key);


    @GET("tours/api_near_locations")
    Single<Response<NearbyPojo>> getNearbyTour(
            @Query("lat") String lat,
            @Query("lon") String lon);


    @GET("cities/get_cities/{abbr}")
    Single<Response<CityPojo>> getCities(@Path("abbr") String abbr);


    @GET("tours/api_search_locations")
    Single<Response<SearchTours>> searchTours(
            @Query("name") String name,
            @Query("state") String state,
            @Query("city") String city);


    @GET("/ToursLocationsImages/api_get_images/{id}")
    Single<Response<ImagesPojo>> getImages(@Path("id") String id);

}
