package com.ss.tourapp.data.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Sammie on 4/10/2017.
 */

public class DetailPojo implements Parcelable {


    public static final Creator<DetailPojo> CREATOR = new Creator<DetailPojo>() {
        @Override
        public DetailPojo createFromParcel(Parcel in) {
            return new DetailPojo(in);
        }

        @Override
        public DetailPojo[] newArray(int size) {
            return new DetailPojo[size];
        }
    };
    private String VideoUrl;
    private LatLng Direction;
    private String ShareText;
    private String Description;
    private String ImageUrl;
    private String TourName;
    private String Id;

    public DetailPojo(String videoUrl, LatLng direction, String shareText, String description, String imageUrl, String tourName, String id) {
        VideoUrl = videoUrl;
        Direction = direction;
        ShareText = shareText;
        Description = description;
        ImageUrl = imageUrl;
        TourName = tourName;
        Id = id;
    }

    protected DetailPojo(Parcel in) {
        VideoUrl = in.readString();
        Direction = in.readParcelable(LatLng.class.getClassLoader());
        ShareText = in.readString();
        Description = in.readString();
        ImageUrl = in.readString();
        TourName = in.readString();
        Id = in.readString();
    }

    public String getVideoUrl() {
        return VideoUrl;
    }

    public LatLng getDirection() {
        return Direction;
    }

    public String getShareText() {
        return ShareText;
    }

    public String getDescription() {
        return Description;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public String getTourName() {
        return TourName;
    }

    public String getId() {
        return Id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(VideoUrl);
        dest.writeParcelable(Direction, flags);
        dest.writeString(ShareText);
        dest.writeString(Description);
        dest.writeString(ImageUrl);
        dest.writeString(TourName);
        dest.writeString(Id);
    }
}
