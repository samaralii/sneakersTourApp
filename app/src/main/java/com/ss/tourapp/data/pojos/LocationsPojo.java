
package com.ss.tourapp.data.pojos;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocationsPojo implements Parcelable {

    public static final Creator<LocationsPojo> CREATOR = new Creator<LocationsPojo>() {
        @Override
        public LocationsPojo createFromParcel(Parcel in) {
            return new LocationsPojo(in);
        }

        @Override
        public LocationsPojo[] newArray(int size) {
            return new LocationsPojo[size];
        }
    };
    @SerializedName("Tour")
    @Expose
    private Tour tour;
    @SerializedName("ToursLocation")
    @Expose
    private List<ToursLocation> toursLocation = null;
    @SerializedName("ToursLocationsImage")
    @Expose
    private List<ToursLocationsImage> toursLocationsImage = null;

    public LocationsPojo() {

    }

    protected LocationsPojo(Parcel in) {
        tour = in.readParcelable(Tour.class.getClassLoader());
        toursLocation = in.createTypedArrayList(ToursLocation.CREATOR);
        toursLocationsImage = in.createTypedArrayList(ToursLocationsImage.CREATOR);
    }

    public Tour getTour() {
        return tour;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }


    public List<ToursLocation> getToursLocation() {
        return toursLocation;
    }

    public void setToursLocation(List<ToursLocation> toursLocation) {
        this.toursLocation = toursLocation;
    }

    public List<ToursLocationsImage> getToursLocationsImage() {
        return toursLocationsImage;
    }

    public void setToursLocationsImage(List<ToursLocationsImage> toursLocationsImage) {
        this.toursLocationsImage = toursLocationsImage;
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeParcelable(tour, flags);
        dest.writeTypedList(toursLocation);
        dest.writeTypedList(toursLocationsImage);
    }

}
