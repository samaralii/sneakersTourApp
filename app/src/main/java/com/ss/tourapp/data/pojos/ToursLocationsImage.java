
package com.ss.tourapp.data.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ToursLocationsImage implements Parcelable {

    public static final Creator<ToursLocationsImage> CREATOR = new Creator<ToursLocationsImage>() {
        @Override
        public ToursLocationsImage createFromParcel(Parcel in) {
            return new ToursLocationsImage(in);
        }

        @Override
        public ToursLocationsImage[] newArray(int size) {
            return new ToursLocationsImage[size];
        }
    };
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("tour_id")
    @Expose
    private String tourId;
    @SerializedName("location_id")
    @Expose
    private String locationId;
    @SerializedName("img_name")
    @Expose
    private String imgName;
    @SerializedName("img_name_lg")
    @Expose
    private String imgNameLg;
    @SerializedName("img_name_md")
    @Expose
    private String imgNameMd;
    @SerializedName("img_name_sm")
    @Expose
    private String imgNameSm;
    @SerializedName("img_caption")
    @Expose
    private String imgCaption;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("created_by")
    @Expose
    private String createdBy;

    protected ToursLocationsImage(Parcel in) {
        id = in.readString();
        userId = in.readString();
        tourId = in.readString();
        locationId = in.readString();
        imgName = in.readString();
        imgNameLg = in.readString();
        imgNameMd = in.readString();
        imgNameSm = in.readString();
        imgCaption = in.readString();
        created = in.readString();
        createdBy = in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTourId() {
        return tourId;
    }

    public void setTourId(String tourId) {
        this.tourId = tourId;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getImgName() {
        return imgName;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName;
    }

    public Object getImgNameLg() {
        return imgNameLg;
    }

    public void setImgNameLg(String imgNameLg) {
        this.imgNameLg = imgNameLg;
    }

    public Object getImgNameMd() {
        return imgNameMd;
    }

    public void setImgNameMd(String imgNameMd) {
        this.imgNameMd = imgNameMd;
    }

    public Object getImgNameSm() {
        return imgNameSm;
    }

    public void setImgNameSm(String imgNameSm) {
        this.imgNameSm = imgNameSm;
    }

    public String getImgCaption() {
        return imgCaption;
    }

    public void setImgCaption(String imgCaption) {
        this.imgCaption = imgCaption;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(userId);
        dest.writeString(tourId);
        dest.writeString(locationId);
        dest.writeString(imgName);
        dest.writeString(imgNameLg);
        dest.writeString(imgNameMd);
        dest.writeString(imgNameSm);
        dest.writeString(imgCaption);
        dest.writeString(created);
        dest.writeString(createdBy);
    }
}
