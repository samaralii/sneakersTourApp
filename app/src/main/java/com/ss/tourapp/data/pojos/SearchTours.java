
package com.ss.tourapp.data.pojos;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchTours {

    @SerializedName("Tour")
    @Expose
    private List<com.ss.tourapp.data.pojos.nearby.Tour> tour = null;

    public List<com.ss.tourapp.data.pojos.nearby.Tour> getTour() {
        return tour;
    }

    public void setTour(List<com.ss.tourapp.data.pojos.nearby.Tour> tour) {
        this.tour = tour;
    }

}
