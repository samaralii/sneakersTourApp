
package com.ss.tourapp.data.pojos.subLocationPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TourLocation {

    @SerializedName("ToursLocation")
    @Expose
    private ToursLocation toursLocation;

//    @SerializedName("0")
//    @Expose
//    private LocationObject object;

    public ToursLocation getToursLocation() {
        return toursLocation;
    }

    public void setToursLocation(ToursLocation toursLocation) {
        this.toursLocation = toursLocation;
    }

//
//    public LocationObject getObject() {
//        return object;
//    }
//
//    public void setObject(LocationObject object) {
//        this.object = object;
//    }
}
