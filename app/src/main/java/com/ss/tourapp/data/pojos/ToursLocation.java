
package com.ss.tourapp.data.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ToursLocation implements Parcelable {

    public final static Creator<ToursLocation> CREATOR = new Creator<ToursLocation>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ToursLocation createFromParcel(Parcel in) {
            ToursLocation instance = new ToursLocation();
            instance.id = ((String) in.readValue((String.class.getClassLoader())));
            instance.userId = ((String) in.readValue((String.class.getClassLoader())));
            instance.tourId = ((String) in.readValue((String.class.getClassLoader())));
            instance.locName = ((String) in.readValue((String.class.getClassLoader())));
            instance.lat = ((String) in.readValue((String.class.getClassLoader())));
            instance.lon = ((String) in.readValue((String.class.getClassLoader())));
            instance.address1 = ((String) in.readValue((String.class.getClassLoader())));
            instance.address2 = in.readValue((Object.class.getClassLoader()));
            instance.city = ((String) in.readValue((String.class.getClassLoader())));
            instance.state = ((String) in.readValue((String.class.getClassLoader())));
            instance.zip = ((String) in.readValue((String.class.getClassLoader())));
            instance.country = in.readValue((Object.class.getClassLoader()));
            instance.description = ((String) in.readValue((String.class.getClassLoader())));
            instance.excerpt = in.readValue((Object.class.getClassLoader()));
            instance.content = in.readValue((Object.class.getClassLoader()));
            instance.imgMain = ((String) in.readValue((String.class.getClassLoader())));
            instance.imgMainLg = in.readValue((Object.class.getClassLoader()));
            instance.imgMainMd = in.readValue((Object.class.getClassLoader()));
            instance.imgMainSm = in.readValue((Object.class.getClassLoader()));
            instance.video = ((String) in.readValue((String.class.getClassLoader())));
            instance.website = in.readValue((Object.class.getClassLoader()));
            instance.facebook = ((String) in.readValue((String.class.getClassLoader())));
            instance.google = ((String) in.readValue((String.class.getClassLoader())));
            instance.twitter = ((String) in.readValue((String.class.getClassLoader())));
            instance.pinterest = ((String) in.readValue((String.class.getClassLoader())));
            instance.instagram = ((String) in.readValue((String.class.getClassLoader())));
            instance.views = ((String) in.readValue((String.class.getClassLoader())));
            instance.created = ((String) in.readValue((String.class.getClassLoader())));
            instance.createdBy = in.readValue((Object.class.getClassLoader()));
            instance.modified = ((String) in.readValue((String.class.getClassLoader())));
            instance.modifiedBy = in.readValue((Object.class.getClassLoader()));
            return instance;
        }

        public ToursLocation[] newArray(int size) {
            return (new ToursLocation[size]);
        }

    };
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("tour_id")
    @Expose
    private String tourId;
    @SerializedName("loc_name")
    @Expose
    private String locName;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lon")
    @Expose
    private String lon;
    @SerializedName("address1")
    @Expose
    private String address1;
    @SerializedName("address2")
    @Expose
    private Object address2;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("zip")
    @Expose
    private String zip;
    @SerializedName("country")
    @Expose
    private Object country;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("excerpt")
    @Expose
    private Object excerpt;
    @SerializedName("content")
    @Expose
    private Object content;
    @SerializedName("img_main")
    @Expose
    private String imgMain;
    @SerializedName("img_main_lg")
    @Expose
    private Object imgMainLg;
    @SerializedName("img_main_md")
    @Expose
    private Object imgMainMd;
    @SerializedName("img_main_sm")
    @Expose
    private Object imgMainSm;
    @SerializedName("video")
    @Expose
    private String video;
    @SerializedName("website")
    @Expose
    private Object website;
    @SerializedName("facebook")
    @Expose
    private String facebook;
    @SerializedName("google")
    @Expose
    private String google;
    @SerializedName("twitter")
    @Expose
    private String twitter;
    @SerializedName("pinterest")
    @Expose
    private String pinterest;
    @SerializedName("instagram")
    @Expose
    private String instagram;
    @SerializedName("views")
    @Expose
    private String views;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("created_by")
    @Expose
    private Object createdBy;
    @SerializedName("modified")
    @Expose
    private String modified;
    @SerializedName("modified_by")
    @Expose
    private Object modifiedBy;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTourId() {
        return tourId;
    }

    public void setTourId(String tourId) {
        this.tourId = tourId;
    }

    public String getLocName() {
        return locName;
    }

    public void setLocName(String locName) {
        this.locName = locName;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public Object getAddress2() {
        return address2;
    }

    public void setAddress2(Object address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public Object getCountry() {
        return country;
    }

    public void setCountry(Object country) {
        this.country = country;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(Object excerpt) {
        this.excerpt = excerpt;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    public String getImgMain() {
        return imgMain;
    }

    public void setImgMain(String imgMain) {
        this.imgMain = imgMain;
    }

    public Object getImgMainLg() {
        return imgMainLg;
    }

    public void setImgMainLg(Object imgMainLg) {
        this.imgMainLg = imgMainLg;
    }

    public Object getImgMainMd() {
        return imgMainMd;
    }

    public void setImgMainMd(Object imgMainMd) {
        this.imgMainMd = imgMainMd;
    }

    public Object getImgMainSm() {
        return imgMainSm;
    }

    public void setImgMainSm(Object imgMainSm) {
        this.imgMainSm = imgMainSm;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public Object getWebsite() {
        return website;
    }

    public void setWebsite(Object website) {
        this.website = website;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getGoogle() {
        return google;
    }

    public void setGoogle(String google) {
        this.google = google;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getPinterest() {
        return pinterest;
    }

    public void setPinterest(String pinterest) {
        this.pinterest = pinterest;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public Object getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Object createdBy) {
        this.createdBy = createdBy;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public Object getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Object modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(userId);
        dest.writeValue(tourId);
        dest.writeValue(locName);
        dest.writeValue(lat);
        dest.writeValue(lon);
        dest.writeValue(address1);
        dest.writeValue(address2);
        dest.writeValue(city);
        dest.writeValue(state);
        dest.writeValue(zip);
        dest.writeValue(country);
        dest.writeValue(description);
        dest.writeValue(excerpt);
        dest.writeValue(content);
        dest.writeValue(imgMain);
        dest.writeValue(imgMainLg);
        dest.writeValue(imgMainMd);
        dest.writeValue(imgMainSm);
        dest.writeValue(video);
        dest.writeValue(website);
        dest.writeValue(facebook);
        dest.writeValue(google);
        dest.writeValue(twitter);
        dest.writeValue(pinterest);
        dest.writeValue(instagram);
        dest.writeValue(views);
        dest.writeValue(created);
        dest.writeValue(createdBy);
        dest.writeValue(modified);
        dest.writeValue(modifiedBy);
    }

    public int describeContents() {
        return 0;
    }

}
