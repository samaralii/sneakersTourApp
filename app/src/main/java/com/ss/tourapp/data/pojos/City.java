
package com.ss.tourapp.data.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class City extends RealmObject {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("cityname")
    @Expose
    private String cityname;
    @SerializedName("code")
    @Expose
    private String code;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
