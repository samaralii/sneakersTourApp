package com.ss.tourapp.data.source;

import android.support.annotation.NonNull;

import com.ss.tourapp.data.pojos.CityPojo;
import com.ss.tourapp.data.pojos.ImagesPojo;
import com.ss.tourapp.data.pojos.LocationsPojo;
import com.ss.tourapp.data.pojos.SearchTours;
import com.ss.tourapp.data.pojos.nearby.NearbyPojo;
import com.ss.tourapp.data.pojos.subLocationPojo.TourLocation;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by Sammie on 3/22/2017.
 */

public interface TaskDataSource {


    @NonNull
    Single<List<LocationsPojo>> getAllLocation(String lat, String lon);

    @NonNull
    Single<List<TourLocation>> getSubLocations(String id);


    @NonNull
    Single<NearbyPojo> getNearbyTour(String lat, String lon);

    @NonNull
    Single<CityPojo> getCitites(String stateAbbr);

    @NonNull
    Single<SearchTours> searchTours(
            String name,
            String city,
            String state);


    @NonNull
    Single<ImagesPojo> getImages(String id);
}
