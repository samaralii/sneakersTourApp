package com.ss.tourapp.data.pojos;

import io.realm.RealmObject;

/**
 * Created by Sammie on 4/11/2017.
 */

public class StatesPojo extends RealmObject {

    private String abbr;
    private String name;

    public StatesPojo(String abbr, String name) {
        this.abbr = abbr;
        this.name = name;
    }

    public StatesPojo() {
    }

    public String getAbbr() {
        return abbr;
    }

    public String getName() {
        return name;
    }
}
