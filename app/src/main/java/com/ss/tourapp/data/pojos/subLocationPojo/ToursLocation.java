
package com.ss.tourapp.data.pojos.subLocationPojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ToursLocation implements Parcelable {

    public static final Creator<ToursLocation> CREATOR = new Creator<ToursLocation>() {
        @Override
        public ToursLocation createFromParcel(Parcel in) {
            return new ToursLocation(in);
        }

        @Override
        public ToursLocation[] newArray(int size) {
            return new ToursLocation[size];
        }
    };
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("loc_name")
    @Expose
    private String locName;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lon")
    @Expose
    private String lon;
    @SerializedName("address1")
    @Expose
    private String address1;
    @SerializedName("address2")
    @Expose
    private String address2;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("zip")
    @Expose
    private String zip;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("excerpt")
    @Expose
    private String excerpt;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("img_main")
    @Expose
    private String imgMain;
    @SerializedName("img_main_md")
    @Expose
    private String imgMainMd;
    @SerializedName("img_main_sm")
    @Expose
    private String imgMainSm;
    @SerializedName("video")
    @Expose
    private String video;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("facebook")
    @Expose
    private String facebook;
    @SerializedName("google")
    @Expose
    private String google;
    @SerializedName("twitter")
    @Expose
    private String twitter;
    @SerializedName("pinterest")
    @Expose
    private String pinterest;
    @SerializedName("instagram")
    @Expose
    private String instagram;

    protected ToursLocation(Parcel in) {
        id = in.readString();
        locName = in.readString();
        lat = in.readString();
        lon = in.readString();
        address1 = in.readString();
        address2 = in.readString();
        city = in.readString();
        state = in.readString();
        zip = in.readString();
        country = in.readString();
        description = in.readString();
        excerpt = in.readString();
        content = in.readString();
        imgMain = in.readString();
        imgMainMd = in.readString();
        imgMainSm = in.readString();
        video = in.readString();
        website = in.readString();
        facebook = in.readString();
        google = in.readString();
        twitter = in.readString();
        pinterest = in.readString();
        instagram = in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocName() {
        return locName;
    }

    public void setLocName(String locName) {
        this.locName = locName;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImgMain() {
        return imgMain;
    }

    public void setImgMain(String imgMain) {
        this.imgMain = imgMain;
    }

    public String getImgMainMd() {
        return imgMainMd;
    }

    public void setImgMainMd(String imgMainMd) {
        this.imgMainMd = imgMainMd;
    }

    public String getImgMainSm() {
        return imgMainSm;
    }

    public void setImgMainSm(String imgMainSm) {
        this.imgMainSm = imgMainSm;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getGoogle() {
        return google;
    }

    public void setGoogle(String google) {
        this.google = google;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getPinterest() {
        return pinterest;
    }

    public void setPinterest(String pinterest) {
        this.pinterest = pinterest;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(locName);
        dest.writeString(lat);
        dest.writeString(lon);
        dest.writeString(address1);
        dest.writeString(address2);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(zip);
        dest.writeString(country);
        dest.writeString(description);
        dest.writeString(excerpt);
        dest.writeString(content);
        dest.writeString(imgMain);
        dest.writeString(imgMainMd);
        dest.writeString(imgMainSm);
        dest.writeString(video);
        dest.writeString(website);
        dest.writeString(facebook);
        dest.writeString(google);
        dest.writeString(twitter);
        dest.writeString(pinterest);
        dest.writeString(instagram);
    }
}
