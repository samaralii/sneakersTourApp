
package com.ss.tourapp.data.pojos.subLocationPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocationObject {

    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("tour_anchor_distance")
    @Expose
    private String tourAnchorDistance;

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTourAnchorDistance() {
        return tourAnchorDistance;
    }

    public void setTourAnchorDistance(String tourAnchorDistance) {
        this.tourAnchorDistance = tourAnchorDistance;
    }

}
