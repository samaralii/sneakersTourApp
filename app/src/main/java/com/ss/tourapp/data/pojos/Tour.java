
package com.ss.tourapp.data.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Tour implements Parcelable {

    public final static Creator<Tour> CREATOR = new Creator<Tour>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Tour createFromParcel(Parcel in) {
            Tour instance = new Tour();
            instance.id = ((String) in.readValue((String.class.getClassLoader())));
            instance.active = ((String) in.readValue((String.class.getClassLoader())));
            instance.status = ((String) in.readValue((String.class.getClassLoader())));
            instance.userId = ((String) in.readValue((String.class.getClassLoader())));
            instance.tourName = ((String) in.readValue((String.class.getClassLoader())));
            instance.description = ((String) in.readValue((String.class.getClassLoader())));
            instance.lat = ((String) in.readValue((String.class.getClassLoader())));
            instance.lon = ((String) in.readValue((String.class.getClassLoader())));
            instance.address1 = ((String) in.readValue((String.class.getClassLoader())));
            instance.address2 = ((String) in.readValue((String.class.getClassLoader())));
            instance.city = ((String) in.readValue((String.class.getClassLoader())));
            instance.state = ((String) in.readValue((String.class.getClassLoader())));
            instance.zip = ((String) in.readValue((String.class.getClassLoader())));
            instance.country = ((String) in.readValue((String.class.getClassLoader())));
            instance.website = ((String) in.readValue((String.class.getClassLoader())));
            instance.facebook = ((String) in.readValue((String.class.getClassLoader())));
            instance.google = ((String) in.readValue((String.class.getClassLoader())));
            instance.twitter = ((String) in.readValue((String.class.getClassLoader())));
            instance.linkedin = ((String) in.readValue((String.class.getClassLoader())));
            instance.pinterest = ((String) in.readValue((String.class.getClassLoader())));
            instance.instagram = ((String) in.readValue((String.class.getClassLoader())));
            instance.views = ((String) in.readValue((String.class.getClassLoader())));
            instance.created = ((String) in.readValue((String.class.getClassLoader())));
            instance.createdBy = ((String) in.readValue((String.class.getClassLoader())));
            instance.modified = ((String) in.readValue((String.class.getClassLoader())));
            instance.modifiedBy = ((String) in.readValue((String.class.getClassLoader())));
            instance.tourImg = ((String) in.readValue((String.class.getClassLoader())));
            instance.tourImgMd = ((String) in.readValue((String.class.getClassLoader())));
            instance.tourImgSm = ((String) in.readValue((String.class.getClassLoader())));
            instance.tourImgXs = ((String) in.readValue((String.class.getClassLoader())));
            instance.video = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public Tour[] newArray(int size) {
            return (new Tour[size]);
        }

    };
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("tour_name")
    @Expose
    private String tourName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lon")
    @Expose
    private String lon;
    @SerializedName("address1")
    @Expose
    private String address1;
    @SerializedName("address2")
    @Expose
    private String address2;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("zip")
    @Expose
    private String zip;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("facebook")
    @Expose
    private String facebook;
    @SerializedName("google")
    @Expose
    private String google;
    @SerializedName("twitter")
    @Expose
    private String twitter;
    @SerializedName("linkedin")
    @Expose
    private String linkedin;
    @SerializedName("pinterest")
    @Expose
    private String pinterest;
    @SerializedName("instagram")
    @Expose
    private String instagram;
    @SerializedName("views")
    @Expose
    private String views;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("modified")
    @Expose
    private String modified;
    @SerializedName("modified_by")
    @Expose
    private String modifiedBy;
    @SerializedName("tour_img")
    @Expose
    private String tourImg;
    @SerializedName("tour_img_md")
    @Expose
    private String tourImgMd;
    @SerializedName("tour_img_sm")
    @Expose
    private String tourImgSm;
    @SerializedName("tour_img_xs")
    @Expose
    private String tourImgXs;
    @SerializedName("video")
    @Expose
    private String video;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTourName() {
        return tourName;
    }

    public void setTourName(String tourName) {
        this.tourName = tourName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getGoogle() {
        return google;
    }

    public void setGoogle(String google) {
        this.google = google;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public String getPinterest() {
        return pinterest;
    }

    public void setPinterest(String pinterest) {
        this.pinterest = pinterest;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getTourImg() {
        return tourImg;
    }

    public void setTourImg(String tourImg) {
        this.tourImg = tourImg;
    }

    public String getTourImgMd() {
        return tourImgMd;
    }

    public void setTourImgMd(String tourImgMd) {
        this.tourImgMd = tourImgMd;
    }

    public String getTourImgSm() {
        return tourImgSm;
    }

    public void setTourImgSm(String tourImgSm) {
        this.tourImgSm = tourImgSm;
    }

    public String getTourImgXs() {
        return tourImgXs;
    }

    public void setTourImgXs(String tourImgXs) {
        this.tourImgXs = tourImgXs;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(active);
        dest.writeValue(status);
        dest.writeValue(userId);
        dest.writeValue(tourName);
        dest.writeValue(description);
        dest.writeValue(lat);
        dest.writeValue(lon);
        dest.writeValue(address1);
        dest.writeValue(address2);
        dest.writeValue(city);
        dest.writeValue(state);
        dest.writeValue(zip);
        dest.writeValue(country);
        dest.writeValue(website);
        dest.writeValue(facebook);
        dest.writeValue(google);
        dest.writeValue(twitter);
        dest.writeValue(linkedin);
        dest.writeValue(pinterest);
        dest.writeValue(instagram);
        dest.writeValue(views);
        dest.writeValue(created);
        dest.writeValue(createdBy);
        dest.writeValue(modified);
        dest.writeValue(modifiedBy);
        dest.writeValue(tourImg);
        dest.writeValue(tourImgMd);
        dest.writeValue(tourImgSm);
        dest.writeValue(tourImgXs);
        dest.writeValue(video);
    }

    public int describeContents() {
        return 0;
    }

}
