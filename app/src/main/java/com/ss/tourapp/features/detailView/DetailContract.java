package com.ss.tourapp.features.detailView;

import com.ss.tourapp.BasePresenter;
import com.ss.tourapp.BaseView;
import com.ss.tourapp.data.pojos.Image;

import java.util.List;

/**
 * Created by Sammie on 3/27/2017.
 */

public interface DetailContract {

    interface presenter extends BasePresenter {

        void getImages(String id);
    }

    interface view extends BaseView<presenter> {

        void onFinally();

        void onSuccessImagesReturn(List<Image> images);

        void onFailedImagesReturn();

        void onNoImagesReturn();
    }
}
