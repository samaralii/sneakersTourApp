package com.ss.tourapp.features.visitView;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.ss.tourapp.R;
import com.ss.tourapp.Utilz;
import com.ss.tourapp.data.pojos.subLocationPojo.TourLocation;
import com.ss.tourapp.data.pojos.subLocationPojo.ToursLocation;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Sammie on 3/27/2017.
 */

public class VisitListAdapter extends RecyclerView.Adapter<VisitListAdapter.toursVh> {


    private List<TourLocation> data;
    private Context context;

    private String img_url;

    private TourListListener listener;

    public VisitListAdapter(List<TourLocation> data, TourListListener listener) {
        this.data = data;
        this.listener = listener;
    }

    @Override
    public toursVh onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_tours, parent, false);
        return new toursVh(v);
    }

    @Override
    public void onBindViewHolder(toursVh holder, int position) {

        try {

            img_url = Utilz.Const.IMG_BASE_URL_2 + data.get(position).getToursLocation().getImgMain();
            Picasso.with(context).load(img_url).into(holder.imageView);

        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.textView.setText(data.get(position).getToursLocation().getLocName());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface TourListListener {
        void onClick(ToursLocation toursLocation);
    }

    public class toursVh extends RecyclerView.ViewHolder {

        @BindView(R.id.list_tours_imageView)
        ImageView imageView;
        @BindView(R.id.list_tours_name)
        TextView textView;

        public toursVh(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener((v) ->
                    listener.onClick(

                            data.get(getLayoutPosition()).getToursLocation())

            );

        }


    }
}
