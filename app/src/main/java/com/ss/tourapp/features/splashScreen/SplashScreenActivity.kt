package com.ss.tourapp.features.splashScreen

import android.content.Intent
import android.os.Bundle
import com.hannesdorfmann.mosby.mvp.MvpActivity
import com.ss.tourapp.R
import com.ss.tourapp.data.source.TaskRepository
import com.ss.tourapp.features.mainView.MainActivity
import com.ss.tourapp.infrastructure.TourApp
import com.ss.tourapp.injection.modules.RxSchedulers
import io.reactivex.Single
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by Sammie on 3/22/2017.
 */

class SplashScreenActivity : MvpActivity<SplashScreenView, SplashScreenPresenter>(), SplashScreenView {

    @Inject lateinit var taskRepo: TaskRepository

    @Inject lateinit var schedulers: RxSchedulers

    override fun createPresenter(): SplashScreenPresenter {

        (application as TourApp).appComponent().Inject(this)

        return SplashScreenPresenter(taskRepo, schedulers)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activiy_splash_screen)


        Single.just("1")
                .subscribeOn(Schedulers.newThread())
                .delay(DURATION.toLong(), TimeUnit.MILLISECONDS)
                .subscribe(Consumer {

                    val i = Intent(this, MainActivity::class.java)
                    startActivity(i)
                    finish()

                })
    }

    companion object {
        private val DURATION = 1000
    }


}
