package com.ss.tourapp.features.visitView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.google.android.gms.maps.model.LatLng;
import com.ss.tourapp.R;
import com.ss.tourapp.Utilz;
import com.ss.tourapp.data.pojos.DetailPojo;
import com.ss.tourapp.data.pojos.LocationsPojo;
import com.ss.tourapp.data.pojos.subLocationPojo.TourLocation;
import com.ss.tourapp.data.pojos.subLocationPojo.ToursLocation;
import com.ss.tourapp.features.detailView.DetailActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Sammie on 3/27/2017.
 */

public class VisitFragment extends Fragment implements VisitContract.view, VisitListAdapter.TourListListener {


    @BindView(R.id.frag_visit_list)
    RecyclerView recyclerView;


    private DialogFragment pd;


    private VisitContract.presenter mPresenter;
    private Unbinder unbinder;

    private LocationsPojo data;


    public static VisitFragment getInstance() {
        return new VisitFragment();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        data = getArguments().getParcelable(Utilz.Const.DATA);

    }


    @Override
    public void onPause() {
        super.onPause();

        if (pd.isVisible())
            pd.dismiss();

        mPresenter.unSubscribe();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_visit, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void setPresenter(VisitContract.presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {

        pd = Utilz.getDialog(getFragmentManager());
        mPresenter.loadSubData(data.getTour().getId());

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.hasFixedSize();

    }

    @Override
    public void onClick(ToursLocation toursLocation) {


        DetailPojo detailPojo = new DetailPojo(
                toursLocation.getVideo(),
                new LatLng(Double.parseDouble(toursLocation.getLat()), Double.parseDouble(toursLocation.getLon())),
                toursLocation.getLocName(),
                toursLocation.getDescription(),
                Utilz.Const.IMG_BASE_URL_2 + toursLocation.getImgMain(),
                toursLocation.getLocName(),
                toursLocation.getId()
        );

        Intent i = new Intent(getActivity(), DetailActivity.class);
        i.putExtra(Utilz.Const.DATA, detailPojo);

        startActivity(i);
    }

    @Override
    public void setErrorMessage(String s) {

        pd.dismiss();
        Utilz.tmsg(getActivity(), s);

    }


    @Override
    public void populateData(List<TourLocation> tourLocations) {
        pd.dismiss();
        VisitListAdapter adapter = new VisitListAdapter(tourLocations, this);
        recyclerView.setAdapter(adapter);

    }
}
