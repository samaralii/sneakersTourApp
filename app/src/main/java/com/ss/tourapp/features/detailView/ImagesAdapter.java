package com.ss.tourapp.features.detailView;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.ss.tourapp.R;
import com.ss.tourapp.data.pojos.Image;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Sammie on 4/11/2017.
 */

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.toursVh> {


    private final List<Image> data;
    private Context context;

    private String img_url;

    private ImagesAdapter.TourListListener listener;

    public ImagesAdapter(List<Image> data, ImagesAdapter.TourListListener listener) {
        this.data = data;
        this.listener = listener;
    }

    @Override
    public ImagesAdapter.toursVh onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_images, parent, false);
        this.context = parent.getContext();
        return new ImagesAdapter.toursVh(v);
    }

    @Override
    public void onBindViewHolder(ImagesAdapter.toursVh holder, int position) {

        try {

            img_url = data.get(position).getImg();
            Picasso.with(context).load(img_url).into(holder.imageView);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface TourListListener {
        void onClick(String img);
    }


    public class toursVh extends RecyclerView.ViewHolder {

        @BindView(R.id.list_images_imageView)
        CircleImageView imageView;

        public toursVh(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener((v) -> listener.onClick(data.get(getLayoutPosition()).getImg()));

        }

    }
}
