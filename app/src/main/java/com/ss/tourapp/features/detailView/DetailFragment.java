package com.ss.tourapp.features.detailView;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.ss.tourapp.DescriptionDialog;
import com.ss.tourapp.R;
import com.ss.tourapp.Utilz;
import com.ss.tourapp.data.pojos.DetailPojo;
import com.ss.tourapp.data.pojos.Image;
import com.ss.tourapp.features.mapView.MapActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class DetailFragment extends Fragment implements DetailContract.view, ImagesAdapter.TourListListener {


    @BindView(R.id.frag_detail_imageView)
    ImageView imageView;
    @BindView(R.id.frag_detail_desc)
    TextView tvDes;

    @BindView(R.id.frag_detail_recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.frag_detail_progressDialog)
    ProgressBar progressBar;


    private Unbinder unbinder;

    private DetailContract.presenter mPresenter;

    private DetailPojo data;

    public static DetailFragment getInstace() {
        return new DetailFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        data = getArguments().getParcelable(Utilz.Const.DATA);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unSubscribe();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        mPresenter.getImages(data.getId());

        try {
            Picasso.with(getContext()).load(data.getImageUrl()).into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
            Utilz.tmsg(getActivity(), "Error loading Image");
        }

        tvDes.setText(data.getDescription());

    }

    @Override
    public void setPresenter(DetailContract.presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.frag_detail_share)
    void onShareClick() {

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, data.getTourName());
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));

    }

    @OnClick(R.id.frag_detail_playVideo)
    void onPlayVideo() {


    }

    @OnClick(R.id.frag_detail_direction)
    void onDirection() {

        Intent i = new Intent(getActivity(), MapActivity.class);
        i.putExtra(Utilz.Const.DATA, data.getDirection());
        startActivity(i);

    }


    @Override
    public void onFinally() {

        if (progressBar.isShown())
            progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onSuccessImagesReturn(List<Image> images) {

        recyclerView.setVisibility(View.VISIBLE);

        ImagesAdapter adapter = new ImagesAdapter(images, this);

        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onFailedImagesReturn() {
        Utilz.tmsg(getActivity(), "Error");
    }

    @Override
    public void onNoImagesReturn() {
        Utilz.tmsg(getActivity(), "No Images Found");
    }

    @Override
    public void onClick(String img) {

        try {
            Picasso.with(getContext()).load(img).into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @OnClick(R.id.frag_detail_desc)
    void onDesClick() {

        DialogFragment dialogFragment = new DescriptionDialog();
        Bundle b = new Bundle();
        b.putString(Utilz.Const.DATA, data.getDescription());
        dialogFragment.setArguments(b);
        dialogFragment.show(getFragmentManager(), "Dialog");

    }
}
