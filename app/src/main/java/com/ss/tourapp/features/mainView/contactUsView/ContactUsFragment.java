package com.ss.tourapp.features.mainView.contactUsView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import com.ss.tourapp.R;
import com.ss.tourapp.Utilz;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.Unbinder;

import static com.ss.tourapp.Utilz.checkField;

/**
 * Created by Sammie on 4/4/2017.
 */

public class ContactUsFragment extends Fragment implements ContactUsContract.view {


    Unbinder unbinder;
    @BindView(R.id.frag_contactUs_firstName)
    EditText etFirstName;
    @BindView(R.id.frag_contactUs_lastName)
    EditText etLastName;
    @BindView(R.id.frag_contactUs_email)
    EditText etEmail;
    @BindView(R.id.frag_contactUs_phoneNumber)
    EditText etPhoneNumber;
    @BindView(R.id.frag_contactUs_reason)
    AppCompatSpinner spinner;
    @BindView(R.id.frag_contactUs_message)
    EditText etMessage;
    private ContactUsContract.presenter mPresenter;

    private String reason;

    private ProgressDialog dialog;

    public static ContactUsFragment getInstance() {
        return new ContactUsFragment();
    }

    @Override
    public void setPresenter(ContactUsContract.presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dialog = Utilz.getDialog(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_contactus, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.reasons_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);


    }


    @OnItemSelected(R.id.frag_contactUs_reason)
    public void onSelectSpinnerItem(int position) {

        String[] reasonList = getResources().getStringArray(R.array.reasons_array);

        reason = reasonList[position];

    }


    @OnClick(R.id.frag_contactUs_btnSend)
    public void onViewClicked() {


        if (!checkField(etFirstName))
            return;

        if (!checkField(etLastName))
            return;

        if (!checkField(etEmail))
            return;

        if (!checkField(etPhoneNumber))
            return;

        if (!checkField(etMessage))
            return;


        dialog.show();

        mPresenter.sendMessage(etFirstName.getText().toString(),
                etLastName.getText().toString(),
                etEmail.getText().toString(),
                etPhoneNumber.getText().toString(),
                etMessage.getText().toString(),
                reason);

    }


    @Override
    public void onSuccessMessageSent() {
        Utilz.tmsg(getActivity(), getString(R.string.successfullySendMessage));


        etFirstName.setText("");
        etLastName.setText("");
        etEmail.setText("");
        etPhoneNumber.setText("");
        etMessage.setText("");

    }

    @Override
    public void onErrorSendingMessage() {
        Utilz.tmsg(getActivity(), "Error");
    }

    @Override
    public void onFinallySendingMessage() {
        dialog.dismiss();
    }
}
