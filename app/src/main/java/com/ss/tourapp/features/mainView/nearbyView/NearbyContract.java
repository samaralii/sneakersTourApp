package com.ss.tourapp.features.mainView.nearbyView;

import com.google.android.gms.maps.model.LatLng;
import com.ss.tourapp.BasePresenter;
import com.ss.tourapp.BaseView;
import com.ss.tourapp.data.pojos.nearby.Tour;

import java.util.List;

/**
 * Created by Sammie on 4/5/2017.
 */

public interface NearbyContract {

    interface view extends BaseView<presenter> {

        void onFinallyNearbyTours();

        void onNoNearbyTourFound();

        void onReturnNearbyTour(List<Tour> tours);

        void onErrorNearbyTour();

    }


    interface presenter extends BasePresenter {

        void loadNearbyTours(LatLng latLng);
    }
}
