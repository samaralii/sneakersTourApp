package com.ss.tourapp.features.visitView;

import com.ss.tourapp.BasePresenter;
import com.ss.tourapp.BaseView;
import com.ss.tourapp.data.pojos.subLocationPojo.TourLocation;

import java.util.List;

/**
 * Created by Sammie on 3/27/2017.
 */

public interface VisitContract {

    interface presenter extends BasePresenter {

        void loadSubData(String id);
    }

    interface view extends BaseView<presenter> {

        void setErrorMessage(String s);

        void populateData(List<TourLocation> tourLocations);
    }
}
