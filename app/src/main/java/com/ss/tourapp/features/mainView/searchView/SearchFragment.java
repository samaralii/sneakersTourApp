package com.ss.tourapp.features.mainView.searchView;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.google.android.gms.maps.model.LatLng;
import com.ss.tourapp.R;
import com.ss.tourapp.Utilz;
import com.ss.tourapp.data.pojos.City;
import com.ss.tourapp.data.pojos.DetailPojo;
import com.ss.tourapp.data.pojos.StatesPojo;
import com.ss.tourapp.data.pojos.nearby.Tour;
import com.ss.tourapp.features.detailView.DetailActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.Unbinder;

/**
 * Created by Sammie on 4/4/2017.
 */

public class SearchFragment extends Fragment implements SearchContract.view, SearchTourAdapter.TourListListener {


    Unbinder unbinder;

    @BindView(R.id.frag_search_etName)
    EditText etName;

    @BindView(R.id.frag_search_recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.frag_search_fields)
    View searchFields;

    @BindView(R.id.frag_search_listView)
    LinearLayout listView;


    @BindView(R.id.frag_search_states)
    Spinner spinnerStates;

    @BindView(R.id.frag_search_city)
    Spinner spinnerCities;


    @BindView(R.id.frag_search_progressbar)
    ProgressBar progressBar;


    private DialogFragment pd;


    private SearchContract.presenter mPresenter;

    private String state;
    private String city;
    private String name;
    private List<StatesPojo> listState;
    private List<City> cityList;

    public static SearchFragment getInstance() {
        return new SearchFragment();
    }

    @Override
    public void setPresenter(SearchContract.presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_search, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
        mPresenter.getStatesList();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

    }


    @OnClick(R.id.frag_search_btnSearch)
    public void onBtnSearchClick() {

        pd = Utilz.getDialog(getFragmentManager());

        mPresenter.searchTours(
                (etName.getText().toString().isEmpty()) ? "" : etName.getText().toString().trim(),
                (state == null) ? "" : state,
                (city == null) ? "" : city);

    }

    @Override
    public void onSuccessSearch(List<Tour> tour) {

        etName.setText("");

        searchFields.setVisibility(View.GONE);
        listView.setVisibility(View.VISIBLE);

        SearchTourAdapter adapter = new SearchTourAdapter(tour, getContext(), this);
        recyclerView.setAdapter(adapter);


    }


    @Override
    public void onClick(Tour tour) {

        DetailPojo detailPojo = new DetailPojo(
                "",
                new LatLng(Double.parseDouble(tour.getLat()), Double.parseDouble(tour.getLon())),
                tour.getTourName(),
                tour.getDes(),
                tour.getImg(),
                tour.getTourName(),
                tour.getId()
        );


        Intent i = new Intent(getActivity(), DetailActivity.class);
        i.putExtra(Utilz.Const.DATA, detailPojo);

        startActivity(i);

    }


    @Override
    public void onErrorSearch() {
        Utilz.tmsg(getActivity(), "Error");
    }

    @Override
    public void onNoTourFound() {
        Utilz.tmsg(getActivity(), "No Tour Found");
    }

    @Override
    public void onFinally() {

        if (pd != null)
            pd.dismiss();

        if (progressBar.isShown()) {
            spinnerCities.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }

    }

    @Override
    public void errorGettingStateList() {
        Utilz.tmsg(getActivity(), "Error Getting States");
    }

    @Override
    public void stateArrayList(List<StatesPojo> statesPojos) {
        listState = statesPojos;
        StateSpinnerAdapter adapter = new StateSpinnerAdapter(getActivity(), listState);
        spinnerStates.setAdapter(adapter);
    }

    @Override
    public void onFailedGettingCities() {
        Utilz.tmsg(getActivity(), "Error getting Cities from server");

    }

    @Override
    public void onNoCityFound() {
        Utilz.tmsg(getActivity(), "No Cities Found");
    }

    @Override
    public void onCitiesReturn(List<City> city) {
        cityList = city;
        CitySpinnerAdapter adapter = new CitySpinnerAdapter(getActivity(), cityList);
        spinnerCities.setAdapter(adapter);
    }

    @OnItemSelected(R.id.frag_search_city)
    void onCitySelect(int position) {


        if (cityList.get(position).getCityname().equalsIgnoreCase(cityList.get(0).getCityname())) {
            city = null;
        } else {
            city = cityList.get(position).getCityname();
        }


    }


    @OnItemSelected(R.id.frag_search_states)
    void onStateSelect(int position) {


        if (listState.get(position).getName().equalsIgnoreCase(listState.get(0).getName())) {
            state = null;
            mPresenter.onNoStateSelected();
        } else {

            state = listState.get(position).getAbbr();
            progressBar.setVisibility(View.VISIBLE);
            spinnerCities.setVisibility(View.GONE);
            mPresenter.onStateSelect(listState.get(position));

        }


    }


    @OnClick(R.id.frag_search_btnSearchAgain)
    public void onSearchAgainClick() {

        searchFields.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);

    }

}
