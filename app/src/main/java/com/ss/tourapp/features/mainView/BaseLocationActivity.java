package com.ss.tourapp.features.mainView;

import android.Manifest;
import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.ss.tourapp.R;
import com.ss.tourapp.Utilz;
import com.tbruyelle.rxpermissions2.RxPermissions;


/**
 * Created by Sammie on 4/26/2017.
 */

public class BaseLocationActivity extends AppCompatActivity implements LocationListener {


    private static final String TAG = BaseLocationActivity.class.getName().toUpperCase();
    private LatLng currentLatlng;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private RxPermissions rxPermissions;
    private AlertDialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rxPermissions = new RxPermissions(this);

        dialog = new AlertDialog.Builder(this)
                .setTitle("Alert")
                .setMessage(R.string.location_message)
                .setPositiveButton("Ok", (d, i) -> d.dismiss())
                .create();
    }

    @Override
    public void onLocationChanged(Location location) {

        setCurrentLatlng(new LatLng(location.getLatitude(), location.getLongitude()));

        Log.d(TAG, "onLocationChanged: " + getCurrentLatlng());

    }

    public LatLng getCurrentLatlng() {
        return currentLatlng;
    }

    private void setCurrentLatlng(LatLng currentLatlng) {
        this.currentLatlng = currentLatlng;
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                Toast.makeText(getApplicationContext(),
                        "This device is supported. Please download google play services", Toast.LENGTH_LONG)
                        .show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }

    public void startFusedLocation() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new
                    GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnectionSuspended(int cause) {
                        }

                        @Override
                        public void onConnected(Bundle connectionHint) {

                        }
                    }).addOnConnectionFailedListener(result -> {

                    }).build();
            mGoogleApiClient.connect();
        } else {
            mGoogleApiClient.connect();
        }
    }


    public void stopFusedLocation() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    public void registerRequestUpdate(final LocationListener listener) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000); // every second

        new Handler().postDelayed(() -> {
            // TODO Auto-generated method stub
            try {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                        mLocationRequest, listener);
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
                if (!isGoogleApiClientConnected()) {
                    mGoogleApiClient.connect();
                }
                registerRequestUpdate(listener);
            }
        }, 1000);
    }

    public boolean isGoogleApiClientConnected() {
        return mGoogleApiClient != null && mGoogleApiClient.isConnected();
    }


    @Override
    protected void onPause() {
        stopFusedLocation();
        super.onPause();

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (checkPlayServices()) {
            startFusedLocation();
            registerRequestUpdate(this);
            checkLocationPermission();
        }
    }


    //ask for permssion
    private void checkLocationPermission() {

        rxPermissions
                .request(Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                .subscribe(granted -> {

                    if (granted) {

                        if (!isLocationEnabled()) {

                            if (!dialog.isShowing())
                                dialog.show();

                        }


                    } else {


                    }
                });
    }


    //Check if location is enable or not
    public boolean isLocationEnabled() {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }

    }

}
