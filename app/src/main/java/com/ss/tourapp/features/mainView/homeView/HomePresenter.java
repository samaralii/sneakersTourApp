package com.ss.tourapp.features.mainView.homeView;

import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;
import com.ss.tourapp.Utilz;
import com.ss.tourapp.data.pojos.LocationsPojo;
import com.ss.tourapp.data.source.TaskDataSource;
import com.ss.tourapp.injection.modules.RxSchedulersInterface;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

import static kotlin.jvm.internal.Intrinsics.checkNotNull;

/**
 * Created by Sammie on 3/22/2017.
 */

public class HomePresenter implements HomeContract.presenter {


    private final TaskDataSource mTaskRepository;
    private HomeContract.view homeView;
    private CompositeDisposable disposable = new CompositeDisposable();
    private RxSchedulersInterface schedulers;


    public HomePresenter(HomeContract.view homeView,
                         TaskDataSource mTaskRepository,
                         RxSchedulersInterface schedulers) {

        this.homeView = homeView;
        this.mTaskRepository = mTaskRepository;
        this.schedulers = schedulers;


    }


    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        disposable.clear();
    }

    @Override
    public void loadData(@NonNull LatLng latLng) {

        checkNotNull(latLng);

        String lat = latLng.latitude + "";
        String lon = latLng.longitude + "";

        disposable.add(mTaskRepository.getAllLocation(lat, lon)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribeWith(new DisposableSingleObserver<List<LocationsPojo>>() {
                    @Override
                    public void onSuccess(List<LocationsPojo> locationsPojos) {

                        if (locationsPojos == null || locationsPojos.isEmpty())
                            homeView.onErrorLoadingLocationData(Utilz.Const.ERROR_NO_TOUR);

                        homeView.onSuccessLoadingLocationData(locationsPojos);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        homeView.onErrorLoadingLocationData(Utilz.Const.ERROR);
                    }
                }));

    }


}
