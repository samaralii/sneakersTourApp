package com.ss.tourapp.features.mainView.nearbyView;

import com.google.android.gms.maps.model.LatLng;
import com.ss.tourapp.data.pojos.nearby.NearbyPojo;
import com.ss.tourapp.data.source.TaskRepository;
import com.ss.tourapp.injection.modules.RxSchedulers;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by Sammie on 4/5/2017.
 */

public class NearbyPresenter implements NearbyContract.presenter {


    private NearbyContract.view view;
    private TaskRepository mTaskRepo;
    private RxSchedulers rxSchedulers;

    private CompositeDisposable disposable = new CompositeDisposable();

    public NearbyPresenter(NearbyContract.view view, TaskRepository mTaskRepo, RxSchedulers rxSchedulers) {

        this.view = view;
        this.mTaskRepo = mTaskRepo;
        this.rxSchedulers = rxSchedulers;
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        disposable.clear();
    }

    @Override
    public void loadNearbyTours(LatLng latLng) {

        String lat = latLng.latitude + "";
        String lon = latLng.longitude + "";

        disposable.add(mTaskRepo.getNearbyTour(lat, lon)
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.ui())
                .doFinally(() -> view.onFinallyNearbyTours())
                .subscribeWith(new DisposableSingleObserver<NearbyPojo>() {
                    @Override
                    public void onSuccess(NearbyPojo nearbyPojo) {

                        if (nearbyPojo == null || nearbyPojo.getTours() == null || nearbyPojo.getTours().isEmpty()) {
                            view.onNoNearbyTourFound();
                            return;
                        }


                        view.onReturnNearbyTour(nearbyPojo.getTours());

                    }

                    @Override
                    public void onError(Throwable throwable) {
                        throwable.printStackTrace();
                        view.onErrorNearbyTour();
                    }
                }));

    }
}
