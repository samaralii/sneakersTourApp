package com.ss.tourapp.features.mapView;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ss.tourapp.R;
import com.ss.tourapp.Utilz;
import com.ss.tourapp.data.source.TaskRepository;
import com.ss.tourapp.infrastructure.TourApp;
import com.ss.tourapp.injection.modules.RxSchedulers;

import javax.inject.Inject;

/**
 * Created by Sammie on 3/29/2017.
 */

public class MapActivity extends AppCompatActivity implements MapContract.view, OnMapReadyCallback {


    @Inject
    TaskRepository repository;
    @Inject
    RxSchedulers schedulers;
    private MapPresenter mapPresenter;
    private GoogleMap googleMap;
    private LatLng camera;
    private MapContract.presenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivity_map);

        ((TourApp) getApplication()).appComponent().Inject(this);

        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);

        camera = getIntent().getParcelableExtra(Utilz.Const.DATA);

        mapPresenter = new MapPresenter(this, repository, schedulers);
        setPresenter(mapPresenter);

    }

    @Override
    public void setPresenter(MapContract.presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(camera, 18));

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(camera);
        googleMap.addMarker(markerOptions);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.unSubscribe();
    }
}
