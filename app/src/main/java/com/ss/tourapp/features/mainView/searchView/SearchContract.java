package com.ss.tourapp.features.mainView.searchView;

import com.ss.tourapp.BasePresenter;
import com.ss.tourapp.BaseView;
import com.ss.tourapp.data.pojos.City;
import com.ss.tourapp.data.pojos.StatesPojo;
import com.ss.tourapp.data.pojos.nearby.Tour;

import java.util.List;

/**
 * Created by Sammie on 4/5/2017.
 */

public interface SearchContract {

    interface view extends BaseView<presenter> {

        void onSuccessSearch(List<Tour> tour);

        void onErrorSearch();

        void onNoTourFound();

        void onFinally();

        void errorGettingStateList();

        void stateArrayList(List<StatesPojo> statesPojos);

        void onFailedGettingCities();

        void onNoCityFound();

        void onCitiesReturn(List<City> city);
    }


    interface presenter extends BasePresenter {

        void searchTours(String name, String state, String city);

        void getStatesList();

        void onStateSelect(StatesPojo statesPojo);

        void onNoStateSelected();
    }


}
