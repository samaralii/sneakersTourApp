package com.ss.tourapp.features.mapView;

import com.ss.tourapp.BasePresenter;
import com.ss.tourapp.BaseView;

/**
 * Created by Sammie on 3/29/2017.
 */

public interface MapContract {

    interface presenter extends BasePresenter {

    }

    interface view extends BaseView<presenter> {

    }

}
