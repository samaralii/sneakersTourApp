package com.ss.tourapp.features.mainView.nearbyView;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.ss.tourapp.R;
import com.ss.tourapp.data.pojos.nearby.Tour;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Sammie on 4/10/2017.
 */

public class NearbyListAdapter extends RecyclerView.Adapter<NearbyListAdapter.toursVh> {


    private final List<Tour> data;
    private Context context;

    private String img_url;

    private NearbyListAdapter.TourListListener listener;

    public NearbyListAdapter(List<Tour> data, Context context, NearbyListAdapter.TourListListener listener) {
        this.data = data;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public NearbyListAdapter.toursVh onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_tours, parent, false);
        return new NearbyListAdapter.toursVh(v);
    }

    @Override
    public void onBindViewHolder(NearbyListAdapter.toursVh holder, int position) {

        try {

            img_url = data.get(position).getImg();
            Picasso.with(context).load(img_url).into(holder.imageView);

        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.textView.setText(data.get(position).getTourName());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface TourListListener {
        void onClick(Tour tour);
    }


    public class toursVh extends RecyclerView.ViewHolder {

        @BindView(R.id.list_tours_imageView)
        ImageView imageView;
        @BindView(R.id.list_tours_name)
        TextView textView;

        public toursVh(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener((v) -> listener.onClick(data.get(getLayoutPosition())));

        }

    }
}
