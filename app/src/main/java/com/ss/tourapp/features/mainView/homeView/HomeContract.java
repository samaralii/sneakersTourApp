package com.ss.tourapp.features.mainView.homeView;

import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;
import com.ss.tourapp.BasePresenter;
import com.ss.tourapp.BaseView;
import com.ss.tourapp.data.pojos.LocationsPojo;

import java.util.List;

/**
 * Created by Sammie on 3/22/2017.
 */

public interface HomeContract {


    interface view extends BaseView<presenter> {

        void onSuccessLoadingLocationData(List<LocationsPojo> locationsPojos);

        void onErrorLoadingLocationData(String errorMessage);

    }


    interface presenter extends BasePresenter {
        void loadData(@NonNull LatLng latLng);
    }

}
