package com.ss.tourapp.features.mapView;

import com.ss.tourapp.data.source.TaskDataSource;
import com.ss.tourapp.injection.modules.RxSchedulers;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Sammie on 3/29/2017.
 */

public class MapPresenter implements MapContract.presenter {


    private MapContract.view mView;
    private TaskDataSource mTaskRepository;
    private RxSchedulers schedulers;

    private CompositeDisposable disposable = new CompositeDisposable();

    public MapPresenter(MapContract.view mView, TaskDataSource mTaskRepository, RxSchedulers schedulers) {
        this.mView = mView;
        this.mTaskRepository = mTaskRepository;
        this.schedulers = schedulers;
    }


    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        disposable.clear();
    }
}
