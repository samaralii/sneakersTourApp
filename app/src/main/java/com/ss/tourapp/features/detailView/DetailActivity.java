package com.ss.tourapp.features.detailView;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.ss.tourapp.R;
import com.ss.tourapp.Utilz;
import com.ss.tourapp.data.pojos.DetailPojo;
import com.ss.tourapp.data.source.TaskRepository;
import com.ss.tourapp.infrastructure.TourApp;
import com.ss.tourapp.injection.modules.RxSchedulers;

import javax.inject.Inject;

public class DetailActivity extends AppCompatActivity {

    @Inject
    TaskRepository taskRepository;

    @Inject
    RxSchedulers schedulers;


    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        toolbar = (Toolbar) findViewById(R.id.app_toolbar);

        setSupportActionBar(toolbar);

        ((TourApp) getApplication()).appComponent().Inject(this);

        DetailFragment fragment = (DetailFragment)
                getSupportFragmentManager().findFragmentById(R.id.activity_detail_container);

        if (fragment == null)
            fragment = DetailFragment.getInstace();

        Bundle b = new Bundle();
        b.putParcelable(Utilz.Const.DATA, getIntent().getParcelableExtra(Utilz.Const.DATA));


        DetailPojo data = getIntent().getParcelableExtra(Utilz.Const.DATA);

        String title = data.getTourName();

        if (title != null)
            setTitle(title);

        Utilz.addFragmentToActivity(getSupportFragmentManager(), fragment, R.id.activity_detail_container, b);

        DetailPresenter presenter = new DetailPresenter(fragment, taskRepository, schedulers);
        fragment.setPresenter(presenter);
    }
}
