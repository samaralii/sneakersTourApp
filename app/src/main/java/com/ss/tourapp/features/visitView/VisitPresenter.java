package com.ss.tourapp.features.visitView;

import com.ss.tourapp.Utilz;
import com.ss.tourapp.data.pojos.subLocationPojo.TourLocation;
import com.ss.tourapp.data.source.TaskDataSource;
import com.ss.tourapp.injection.modules.RxSchedulersInterface;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by Sammie on 3/27/2017.
 */

public class VisitPresenter implements VisitContract.presenter {

    private final VisitContract.view mView;
    private final TaskDataSource mTaskRepository;
    private final RxSchedulersInterface schedulers;
    private CompositeDisposable disposable = new CompositeDisposable();

    public VisitPresenter(VisitContract.view mView,
                          TaskDataSource mTaskRepository, RxSchedulersInterface schedulers) {
        this.mView = mView;
        this.mTaskRepository = mTaskRepository;
        this.schedulers = schedulers;
    }


    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        disposable.clear();
    }


    @Override
    public void loadSubData(String id) {

        if (id == null) {
            mView.setErrorMessage("No Id Found");
            return;
        }


        mTaskRepository.getSubLocations(id)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribeWith(new DisposableSingleObserver<List<TourLocation>>() {
                    @Override
                    public void onSuccess(List<TourLocation> tourLocations) {

                        if (tourLocations == null || tourLocations.isEmpty()) {
                            mView.setErrorMessage(Utilz.Const.ERROR_NO_TOUR);
                            return;
                        }

                        mView.populateData(tourLocations);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        mView.setErrorMessage(Utilz.Const.ERROR);
                    }
                });

    }
}
