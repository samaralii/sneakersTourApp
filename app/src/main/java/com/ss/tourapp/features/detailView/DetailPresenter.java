package com.ss.tourapp.features.detailView;

import com.ss.tourapp.data.pojos.ImagesPojo;
import com.ss.tourapp.data.source.TaskDataSource;
import com.ss.tourapp.injection.modules.RxSchedulers;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by Sammie on 3/27/2017.
 */

public class DetailPresenter implements DetailContract.presenter {


    private final TaskDataSource mTaskRepository;
    private final RxSchedulers schedulers;
    private DetailContract.view mView;
    private CompositeDisposable disposable = new CompositeDisposable();


    public DetailPresenter(DetailContract.view mView, TaskDataSource mTaskRepository, RxSchedulers schedulers) {
        this.mView = mView;
        this.mTaskRepository = mTaskRepository;
        this.schedulers = schedulers;
    }


    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        disposable.clear();
    }

    @Override
    public void getImages(String id) {

        disposable.add(mTaskRepository.getImages(id)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally(mView::onFinally)
                .subscribeWith(new DisposableSingleObserver<ImagesPojo>() {
                    @Override
                    public void onSuccess(ImagesPojo imagesPojo) {

                        if (imagesPojo == null || imagesPojo.getImages().isEmpty()) {
                            mView.onNoImagesReturn();
                            return;
                        }


                        mView.onSuccessImagesReturn(imagesPojo.getImages());

                    }

                    @Override
                    public void onError(Throwable throwable) {
                        throwable.printStackTrace();
                        mView.onFailedImagesReturn();
                    }
                }));

    }
}
