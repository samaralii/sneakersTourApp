package com.ss.tourapp.features.mainView;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;

import com.ss.tourapp.R;
import com.ss.tourapp.Utilz;
import com.ss.tourapp.data.source.TaskRepository;
import com.ss.tourapp.features.mainView.contactUsView.ContactUsFragment;
import com.ss.tourapp.features.mainView.contactUsView.ContactUsPresenter;
import com.ss.tourapp.features.mainView.homeView.HomeFragment;
import com.ss.tourapp.features.mainView.homeView.HomePresenter;
import com.ss.tourapp.features.mainView.nearbyView.NearbyFragment;
import com.ss.tourapp.features.mainView.nearbyView.NearbyPresenter;
import com.ss.tourapp.features.mainView.searchView.SearchFragment;
import com.ss.tourapp.features.mainView.searchView.SearchPresenter;
import com.ss.tourapp.infrastructure.TourApp;
import com.ss.tourapp.injection.modules.RxSchedulers;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Sammie on 3/22/2017.
 */

public class MainActivity extends BaseLocationActivity {


    @Inject
    TaskRepository mTaskRepo;
    @Inject
    RxSchedulers rxSchedulers;

    @BindView(R.id.activity_main_bottom)
    BottomNavigationView bottomNavigationView;

    private int containerId;

    private HomeFragment homeView = null;
    private NearbyFragment nearbyView = null;
    private SearchFragment searchView = null;
    private ContactUsFragment contactUsView = null;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        ((TourApp) getApplication()).appComponent().Inject(this);


        containerId = R.id.activity_main_container;

        //open home fragment first
        openHomeFrag();

        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {

            switch (item.getItemId()) {
                case R.id.action_home:
                    openHomeFrag();
                    break;

                case R.id.action_nearby:
                    openNearbyFrag();
                    break;

                case R.id.action_search:
                    openSearchFrag();
                    break;

                case R.id.action_contactus:
                    openContactUsFrag();
                    break;
            }

            return true;
        });


    }

    private void openHomeFrag() {


        if (homeView == null)
            homeView = HomeFragment.getInstance();

        replaceFragment(homeView);
        HomePresenter presenter = new HomePresenter(homeView, mTaskRepo, rxSchedulers);
        homeView.setPresenter(presenter);


    }


    private void openContactUsFrag() {


        if (contactUsView == null)
            contactUsView = ContactUsFragment.getInstance();

        replaceFragment(contactUsView);
        ContactUsPresenter presenter = new ContactUsPresenter(contactUsView, mTaskRepo, rxSchedulers);
        contactUsView.setPresenter(presenter);


    }

    private void openNearbyFrag() {


        if (nearbyView == null)
            nearbyView = NearbyFragment.getInstance();

        replaceFragment(nearbyView);
        NearbyPresenter presenter = new NearbyPresenter(nearbyView, mTaskRepo, rxSchedulers);
        nearbyView.setPresenter(presenter);


    }

    private void openSearchFrag() {


        if (searchView == null)
            searchView = SearchFragment.getInstance();

        replaceFragment(searchView);
        SearchPresenter presenter = new SearchPresenter(searchView, mTaskRepo, rxSchedulers);
        searchView.setPresenter(presenter);


    }


    void replaceFragment(Fragment fragment) {
        Utilz.replaceFragmentToActivity(getSupportFragmentManager(), fragment, containerId);
    }
}
