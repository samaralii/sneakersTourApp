package com.ss.tourapp.features.mainView.contactUsView;

import com.ss.tourapp.data.source.TaskRepository;
import com.ss.tourapp.injection.modules.RxSchedulers;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Sammie on 4/5/2017.
 */

public class ContactUsPresenter implements ContactUsContract.presenter {


    private final ContactUsContract.view mView;
    private final TaskRepository mTaskRepo;
    private final RxSchedulers rxSchedulers;

    CompositeDisposable disposable = new CompositeDisposable();

    public ContactUsPresenter(ContactUsContract.view mView, TaskRepository mTaskRepo, RxSchedulers rxSchedulers) {

        this.mView = mView;
        this.mTaskRepo = mTaskRepo;
        this.rxSchedulers = rxSchedulers;
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        disposable.clear();
    }

    @Override
    public void sendMessage(String firstName, String lastName,
                            String email, String phoneNumber, String message, String reason) {

        mView.onSuccessMessageSent();
        mView.onErrorSendingMessage();
        mView.onFinallySendingMessage();

    }
}
