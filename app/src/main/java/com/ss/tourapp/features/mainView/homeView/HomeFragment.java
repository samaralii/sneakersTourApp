package com.ss.tourapp.features.mainView.homeView;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.model.LatLng;
import com.ss.tourapp.R;
import com.ss.tourapp.Utilz;
import com.ss.tourapp.data.pojos.LocationsPojo;
import com.ss.tourapp.features.mainView.BaseLocationActivity;
import com.ss.tourapp.features.visitView.VisitActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Sammie on 3/22/2017.
 */

public class HomeFragment extends Fragment implements HomeContract.view, ToursListAdapter.TourListListener {


    @BindView(R.id.main_frag_list)
    RecyclerView recyclerView;


    private HomeContract.presenter mPresenter;
    private Unbinder unbinder;


    private DialogFragment pd;



    public static HomeFragment getInstance() {
        return new HomeFragment();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unSubscribe();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
        mPresenter.loadData(getCurrenctLatLng());
    }


    private LatLng getCurrenctLatLng() {

        LatLng currentLatlng = ((BaseLocationActivity) getActivity()).getCurrentLatlng();

        if (currentLatlng == null) {
            currentLatlng = new LatLng(0, 0);
        }

        return currentLatlng;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void setPresenter(HomeContract.presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {

        pd = Utilz.getDialog(getFragmentManager());


        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.hasFixedSize();
    }


    @Override
    public void onSuccessLoadingLocationData(List<LocationsPojo> locationsPojos) {
        pd.dismiss();
        ToursListAdapter adapter = new ToursListAdapter(locationsPojos, getContext(), this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onErrorLoadingLocationData(String error) {
        pd.dismiss();
        Utilz.tmsg(getActivity(), error);
    }


    @Override
    public void onClick(LocationsPojo locationsPojo) {
        Intent i = new Intent(getActivity(), VisitActivity.class);
        i.putExtra(Utilz.Const.DATA, locationsPojo);
        startActivity(i);
    }
}
