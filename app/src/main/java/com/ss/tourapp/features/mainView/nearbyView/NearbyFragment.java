package com.ss.tourapp.features.mainView.nearbyView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.google.android.gms.maps.model.LatLng;
import com.ss.tourapp.R;
import com.ss.tourapp.Utilz;
import com.ss.tourapp.data.pojos.DetailPojo;
import com.ss.tourapp.data.pojos.nearby.Tour;
import com.ss.tourapp.features.detailView.DetailActivity;
import com.ss.tourapp.features.mainView.BaseLocationActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Sammie on 4/4/2017.
 */

public class NearbyFragment extends Fragment implements NearbyContract.view, NearbyListAdapter.TourListListener {


    Unbinder unbinder;

    @BindView(R.id.frag_nearby_recyclerView)
    RecyclerView recyclerView;

    private DialogFragment pd;

    private NearbyContract.presenter mPresenter;

    private AlertDialog alertDialog;

    public static NearbyFragment getInstance() {
        return new NearbyFragment();
    }

    @Override
    public void setPresenter(NearbyContract.presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_nearby, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.unSubscribe();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private LatLng getCurrenctLatLng() {


        LatLng currentLatlng = ((BaseLocationActivity) getActivity()).getCurrentLatlng();

        if (currentLatlng == null) {
            currentLatlng = new LatLng(0, 0);
        }

        return currentLatlng;

    }

    private void init() {


        alertDialog = new AlertDialog.Builder(getActivity())
                .setTitle("Alert")
                .setMessage(R.string.location_message)
                .setPositiveButton("Ok",
                        (d, i) -> {
                            pd = Utilz.getDialog(getFragmentManager());
                            mPresenter.loadNearbyTours(getCurrenctLatLng());
                            d.dismiss();
                        })
                .create();


        if (!((BaseLocationActivity) getActivity()).isLocationEnabled()) {
            if (!alertDialog.isShowing())
                alertDialog.show();
        } else {
            pd = Utilz.getDialog(getFragmentManager());
            mPresenter.loadNearbyTours(getCurrenctLatLng());
        }


        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

    }


    @Override
    public void onFinallyNearbyTours() {

        pd.dismiss();


    }

    @Override
    public void onNoNearbyTourFound() {
        Utilz.tmsg(getActivity(), "No Nearby Tours Found");
    }

    @Override
    public void onReturnNearbyTour(List<Tour> tours) {
        NearbyListAdapter adapter = new NearbyListAdapter(tours, getActivity(), this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onErrorNearbyTour() {
        Utilz.tmsg(getActivity(), "No Nearby Tours Found");
    }

    @Override
    public void onClick(Tour tour) {

        DetailPojo detailPojo = new DetailPojo(
                "", new LatLng(Double.parseDouble(tour.getLat()), Double.parseDouble(tour.getLon())),
                tour.getTourName(),
                tour.getDes(),
                tour.getImg(),
                tour.getTourName(),
                tour.getId()
        );


        Intent i = new Intent(getActivity(), DetailActivity.class);
        i.putExtra(Utilz.Const.DATA, detailPojo);

        startActivity(i);


    }
}
