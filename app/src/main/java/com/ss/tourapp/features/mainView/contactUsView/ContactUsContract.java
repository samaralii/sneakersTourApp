package com.ss.tourapp.features.mainView.contactUsView;

import com.ss.tourapp.BasePresenter;
import com.ss.tourapp.BaseView;

/**
 * Created by Sammie on 4/5/2017.
 */

public interface ContactUsContract {


    interface view extends BaseView<presenter> {

        void onSuccessMessageSent();

        void onErrorSendingMessage();

        void onFinallySendingMessage();
    }


    interface presenter extends BasePresenter {

        void sendMessage(String firstName, String lastName, String email,
                         String phoneNumber, String message, String reason);
    }


}
