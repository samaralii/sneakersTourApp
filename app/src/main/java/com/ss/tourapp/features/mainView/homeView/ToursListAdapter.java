package com.ss.tourapp.features.mainView.homeView;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.ss.tourapp.R;
import com.ss.tourapp.Utilz;
import com.ss.tourapp.data.pojos.LocationsPojo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Sammie on 3/24/2017.
 */

public class ToursListAdapter extends RecyclerView.Adapter<ToursListAdapter.toursVh> {


    private final List<LocationsPojo> data;
    private Context context;

    private String img_url;

    private TourListListener listener;

    public ToursListAdapter(List<LocationsPojo> data, Context context, TourListListener listener) {
        this.data = data;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public toursVh onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_tours, parent, false);
        return new toursVh(v);
    }

    @Override
    public void onBindViewHolder(toursVh holder, int position) {

        try {

            img_url = Utilz.Const.IMG_BASE_URL + data.get(position).getTour().getTourImgXs();
            Picasso.with(context).load(img_url).into(holder.imageView);

        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.textView.setText(data.get(position).getTour().getTourName());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface TourListListener {
        void onClick(LocationsPojo locationsPojo);
    }

    public class toursVh extends RecyclerView.ViewHolder {

        @BindView(R.id.list_tours_imageView)
        ImageView imageView;
        @BindView(R.id.list_tours_name)
        TextView textView;

        public toursVh(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener((v) -> listener.onClick(data.get(getLayoutPosition())));

        }


    }
}
