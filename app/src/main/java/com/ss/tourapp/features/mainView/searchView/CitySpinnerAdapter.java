package com.ss.tourapp.features.mainView.searchView;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ss.tourapp.R;
import com.ss.tourapp.data.pojos.City;

import java.util.List;

/**
 * Created by Sammie on 4/11/2017.
 */

public class CitySpinnerAdapter extends BaseAdapter {

    Activity context;
    List<City> data;

    public CitySpinnerAdapter(Activity activity, List<City> data) {
        this.context = activity;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, parent);
    }

    @Override
    public City getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, parent);
    }

    private View getCustomView(int position, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.spinner_wallet_guids, parent, false);

        TextView tvStateName = (TextView) view.findViewById(R.id.tvSateName);

        City state = data.get(position);

        tvStateName.setText(state.getCityname());

        return view;
    }

}
