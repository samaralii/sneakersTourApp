package com.ss.tourapp.features.visitView;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.ss.tourapp.R;
import com.ss.tourapp.Utilz;
import com.ss.tourapp.data.source.TaskRepository;
import com.ss.tourapp.infrastructure.TourApp;
import com.ss.tourapp.injection.modules.RxSchedulers;

import javax.inject.Inject;

/**
 * Created by Sammie on 3/27/2017.
 */

public class VisitActivity extends AppCompatActivity {


    @Inject
    TaskRepository taskRepository;

    @Inject
    RxSchedulers schedulers;

    private VisitPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit);

        ((TourApp) getApplication()).appComponent().Inject(this);


        VisitFragment fragment = (VisitFragment)
                getSupportFragmentManager().findFragmentById(R.id.activity_visit_container);

        if (fragment == null)
            fragment = VisitFragment.getInstance();

        Bundle b = new Bundle();
        b.putParcelable(Utilz.Const.DATA, getIntent().getParcelableExtra(Utilz.Const.DATA));


        Utilz.addFragmentToActivity(getSupportFragmentManager(), fragment, R.id.activity_visit_container, b);

        mPresenter = new VisitPresenter(fragment, taskRepository, schedulers);
        fragment.setPresenter(mPresenter);

    }
}
