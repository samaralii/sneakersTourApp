package com.ss.tourapp.features.mainView.searchView;

import com.ss.tourapp.Utilz;
import com.ss.tourapp.data.pojos.City;
import com.ss.tourapp.data.pojos.CityPojo;
import com.ss.tourapp.data.pojos.SearchTours;
import com.ss.tourapp.data.pojos.StatesPojo;
import com.ss.tourapp.data.source.TaskRepository;
import com.ss.tourapp.injection.modules.RxSchedulers;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by Sammie on 4/5/2017.
 */

public class SearchPresenter implements SearchContract.presenter {


    private final static String SELECT_STATE = "Select City";
    private final SearchContract.view mView;
    private final TaskRepository mTaskRepo;
    private final RxSchedulers rxSchedulers;
    private CompositeDisposable disposable = new CompositeDisposable();


    public SearchPresenter(SearchContract.view mView, TaskRepository mTaskRepo, RxSchedulers rxSchedulers) {

        this.mView = mView;
        this.mTaskRepo = mTaskRepo;
        this.rxSchedulers = rxSchedulers;
    }

    @Override
    public void subscribe() {
    }

    @Override
    public void unSubscribe() {
        disposable.clear();
    }

    @Override
    public void searchTours(String name, String state, String city) {

        disposable.add(mTaskRepo.searchTours(name, city, state)
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.ui())
                .doFinally(mView::onFinally)
                .subscribeWith(new DisposableSingleObserver<SearchTours>() {
                    @Override
                    public void onSuccess(SearchTours searchTours) {

                        if (searchTours == null || searchTours.getTour().isEmpty()) {
                            mView.onNoTourFound();
                            return;
                        }

                        mView.onSuccessSearch(searchTours.getTour());
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        throwable.printStackTrace();
                        mView.onErrorSearch();
                    }

                }));

    }

    @Override
    public void getStatesList() {


        disposable.add(Utilz.getStates()
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.ui())
                .subscribeWith(new DisposableSingleObserver<List<StatesPojo>>() {
                    @Override
                    public void onSuccess(List<StatesPojo> statesPojos) {
                        mView.stateArrayList(statesPojos);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        throwable.printStackTrace();
                        mView.errorGettingStateList();
                    }
                }));

    }

    @Override
    public void onStateSelect(StatesPojo statesPojo) {

        disposable.add(mTaskRepo.getCitites(statesPojo.getAbbr())
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.ui())
                .doFinally(mView::onFinally)
                .subscribeWith(new DisposableSingleObserver<CityPojo>() {
                    @Override
                    public void onSuccess(CityPojo cityPojo) {


                        if (cityPojo == null || cityPojo.getCity().isEmpty()) {
                            mView.onNoCityFound();
                            return;
                        }


                        List<City> cityList;
                        cityList = cityPojo.getCity();
                        City city = new City();
                        city.setCityname(SELECT_STATE);
                        cityList.add(0, city);

                        mView.onCitiesReturn(cityList);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        throwable.printStackTrace();
                        mView.onFailedGettingCities();
                    }
                }));

    }

    @Override
    public void onNoStateSelected() {

        List<City> cityList = new ArrayList<>();
        City city = new City();
        city.setCityname(SELECT_STATE);
        cityList.add(0, city);

        mView.onCitiesReturn(cityList);

    }

}
