package com.ss.tourapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.widget.EditText;
import android.widget.Toast;

import com.ss.tourapp.data.pojos.StatesPojo;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;

/**
 * Created by Sammie on 3/22/2017.
 */

public class Utilz {


    public static void tmsg(@NonNull Activity activity, @NonNull String message) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }

    public static void addFragmentToActivity(@NonNull FragmentManager manager,
                                             @NonNull Fragment fragment, @IdRes int frameId) {

        FragmentTransaction ft = manager.beginTransaction();
        ft.add(frameId, fragment);
        ft.commit();

    }

    public static void addFragmentToActivity(@NonNull FragmentManager manager,
                                             @NonNull Fragment fragment, @IdRes int frameId, @NonNull Bundle b) {

        fragment.setArguments(b);
        addFragmentToActivity(manager, fragment, frameId);

    }

    public static void replaceFragmentToActivity(@NonNull FragmentManager manager,
                                                 @NonNull Fragment fragment, @IdRes int frameId) {

        FragmentTransaction ft = manager.beginTransaction();
        ft.replace(frameId, fragment);
        ft.commit();

    }

    public static void replaceFragmentToActivity(@NonNull FragmentManager manager,
                                                 @NonNull Fragment fragment, @IdRes int frameId, @NonNull Bundle b) {

        fragment.setArguments(b);
        addFragmentToActivity(manager, fragment, frameId);

    }


    public static ProgressDialog getDialog(Context context) {
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.setMessage("loading...");
        return dialog;
    }

    public static boolean checkField(EditText editText) {

        if (editText.getText().toString().isEmpty()) {
            editText.setError("Required Field");
            return false;
        } else {
            editText.setError(null);
            return true;
        }

    }


    public static void showAlert(Context context, String title, String message) {

        AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", (d, i) -> d.dismiss())
                .create();

        dialog.show();

    }


    public static Single<List<StatesPojo>> getStates() {
        return Single.fromCallable(Utilz::getStateList);
    }

    public static CustomProgressDialog getDialog(FragmentManager manager) {
        CustomProgressDialog dialogFragment = new CustomProgressDialog();
        dialogFragment.show(manager, "dialog");
        dialogFragment.setCancelable(false);
        return dialogFragment;
    }

    public static List<StatesPojo> getStateList() {

        List<StatesPojo> list = new ArrayList<>();

        list.add(new StatesPojo("NN", "Select State"));
        list.add(new StatesPojo("AL", "Alabama"));
        list.add(new StatesPojo("AK", "Alaska"));
        list.add(new StatesPojo("AZ", "Arizona"));
        list.add(new StatesPojo("AR", "Arkansas"));
        list.add(new StatesPojo("CA", "California"));
        list.add(new StatesPojo("CO", "Colorado"));
        list.add(new StatesPojo("CT", "Connecticut"));
        list.add(new StatesPojo("DE", "Delaware"));
        list.add(new StatesPojo("DC", "District of Columbia"));
        list.add(new StatesPojo("FL", "Florida"));
        list.add(new StatesPojo("GA", "Georgia"));
        list.add(new StatesPojo("HI", "Hawaii"));
        list.add(new StatesPojo("ID", "Idaho"));
        list.add(new StatesPojo("IL", "Illinois"));
        list.add(new StatesPojo("IN", "Indiana"));
        list.add(new StatesPojo("IA", "Iowa"));
        list.add(new StatesPojo("KS", "Kansas"));
        list.add(new StatesPojo("KY", "Kentucky"));
        list.add(new StatesPojo("LA", "Louisiana"));
        list.add(new StatesPojo("ME", "Maine"));
        list.add(new StatesPojo("MD", "Maryland"));
        list.add(new StatesPojo("MA", "Massachusetts"));
        list.add(new StatesPojo("MI", "Michigan"));
        list.add(new StatesPojo("MN", "Minnesota"));
        list.add(new StatesPojo("MS", "Mississippi"));
        list.add(new StatesPojo("MO", "Missouri"));
        list.add(new StatesPojo("MT", "Montana"));
        list.add(new StatesPojo("NE", "Nebraska"));
        list.add(new StatesPojo("NV", "Nevada"));
        list.add(new StatesPojo("NH", "New Hampshire"));
        list.add(new StatesPojo("NJ", "New Jersey"));
        list.add(new StatesPojo("NM", "New Mexico"));
        list.add(new StatesPojo("NY", "New York"));
        list.add(new StatesPojo("NC", "North Carolina"));
        list.add(new StatesPojo("ND", "North Dakota"));
        list.add(new StatesPojo("OH", "Ohio"));
        list.add(new StatesPojo("OK", "Oklahoma"));
        list.add(new StatesPojo("OR", "Oregon"));
        list.add(new StatesPojo("PA", "Pennsylvania"));
        list.add(new StatesPojo("RI", "Rhode Island"));
        list.add(new StatesPojo("SC", "South Carolina"));
        list.add(new StatesPojo("SD", "South Dakota"));
        list.add(new StatesPojo("TN", "Tennessee"));
        list.add(new StatesPojo("TX", "Texas"));
        list.add(new StatesPojo("UT", "Utah"));
        list.add(new StatesPojo("VT", "Vermont"));
        list.add(new StatesPojo("VA", "Virginia"));
        list.add(new StatesPojo("WA", "Washington"));
        list.add(new StatesPojo("WV", "West Virginia"));
        list.add(new StatesPojo("WI", "Wisconsin"));
        list.add(new StatesPojo("WY", "Wyoming"));


        return list;
    }


    public class Const {
        public static final String BASE_URL = "https://sneakertours.com/";
        public static final String IMG_BASE_URL = "https://sneakertours.com/img/tours/";
        public static final String IMG_BASE_URL_2 = "https://sneakertours.com//img/tours_locations/";
        public final static String GOOGLE_MAP_URL = "https://maps.googleapis.com/maps/api/directions/";
        public static final String DATA = "data";

        //Error Message
        public static final String ERROR_NO_TOUR = "No Tour Found";
        public static final String ERROR_NO_INTERNET = "No Location Found";
        public static final String ERROR = "ERROR";


    }
}
