package com.ss.tourapp

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder

/**
 * Created by Sammie on 5/16/2017.
 */
class DescriptionDialog : DialogFragment() {


    var descriptionText: String? = null

    lateinit var unbinder: Unbinder

    @BindView(R.id.dialog_des_text)
    lateinit var desText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        descriptionText = arguments?.getString(Utilz.Const.DATA)
    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater?.inflate(R.layout.dialog_des, container, false) as View
        unbinder = ButterKnife.bind(this, v)
        return v
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        desText.text = descriptionText
        dialog?.setTitle("Description")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder.unbind()
    }

    @OnClick(R.id.dialog_des_btnOk)
    fun onOkClick() {
        dialog?.dismiss()
    }

}