package com.ss.tourapp;

/**
 * Created by Sammie on 3/22/2017.
 */

public interface BaseView<T> {
    void setPresenter(T presenter);
}
