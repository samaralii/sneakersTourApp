package com.ss.tourapp;

/**
 * Created by Sammie on 3/22/2017.
 */

public interface BasePresenter {
    void subscribe();
    void unSubscribe();
}
